package ob.traxe;

import ob.traxe.ParameterDefinitions.ParameterLaneDef;

class TrackerConfiguration {
	public static function DefaultTracks():Array<Track> {
		var numTracks = 16;
		return [for (i in 0...numTracks) new Track(i, getParameters([1, 2]), 16)];
	}

	public static function DrumSynthTracks():Array<Track> {
		var numTracks = 3;
		return [for (i in 0...numTracks) new Track(i, getParameters([2]), 16)];
	}

	public static function SevenOhSeven():Array<Track> {
		return [
			//
			new Track(0, "Kick 1", getParameters([2]), 4),
			//
			new Track(1, "Kick 2", getParameters([2]), 8),
			//
			new Track(2, "Snare 1", getParameters([2]), 16),
			//
			new Track(3, "Snare 2", getParameters([2]), 16),
			//
			new Track(4, "Tom Low", getParameters([2]), 16),
			//
			new Track(5, "Tom Med", getParameters([2]), 16),
			//
			new Track(6, "Tom High", getParameters([2]), 16),
			//
			new Track(7, "Rimshot", getParameters([2]), 7),
			//
			new Track(8, "Cowbell", getParameters([2]), 16),
			//
			new Track(9, "Handclap", getParameters([2]), 16),
			//
			new Track(10, "Tambourine", getParameters([2]), 16),
			//
			new Track(11, "Hi Hat Closed", getParameters([2]), 16),
			//
			new Track(12, "Hi Hat Open", getParameters([2]), 16),
			//
			new Track(13, "Cymbal Crash", getParameters([2]), 16),
			//
			// new Track(14, "Cymbal Ride", getParameters([2]))
		];
	}

	public static function getParameters(parameterDefinitionIndexes:Array<Int>):Array<ParameterLaneDef> {
		return [for (p in parameterDefinitionIndexes) ParameterDefinitions.map[p]];
	}
}

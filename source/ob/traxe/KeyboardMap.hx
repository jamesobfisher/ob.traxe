package ob.traxe;

import ob.traxe.KeyNums.KeyName;

typedef KeyboardMap = {
    name:String,
    noteKeys:Map<Int,KeyName>,
    numberKeys:Map<Int,KeyName>,
    alterationKeys:Map<Int,KeyName>,
    navigationKeys:Map<Int,KeyName>,
    appKeys:Map<Int,KeyName>
}

class Keyboard{
    public static function loadDefaultOpenFlMap():KeyboardMap{
        return {
            name: "en-GB",
            noteKeys:[
                90 => C,
                83 => CSharp,
                88 => D,
                68 => DSharp,
                67 => E,
                86 => F,
                71 => FSharp,
                66 => G,
                72 => GSharp,
                78 => A,
                74 => ASharp,
                77 => B,
                81 => CUp,
                50 => CSharpUp,
                87 => DUp,
                51 => DSharpUp,
                69 => EUp,
                82 => FUp,
                53 => FSharpUp,
                84 => GUp,
                54 => GSharpUp,
                89 => AUp,
                55 => ASharpUp,
                85 => BUp
            ],
            numberKeys:[
                48 => Num0,
                49 => Num1,
                50 => Num2,
                51 => Num3,
                52 => Num4,
                53 => Num5,
                54 => Num6,
                55 => Num7,
                56 => Num8,
                57 => Num9,
                65 => NumA,
                66 => NumB,
                67 => NumC,
                68 => NumD,
                69 => NumE,
                70 => NumF
            ],
            navigationKeys: [
                38 => Up,
                39 => Right,
                37 => Left,
                40 => Down,
                9 => Tab, 
                33 => ScrollUp,
                34 => ScrollDown,
                36 => ScrollTop,
                35 => ScrollEnd,
            ],
            alterationKeys:[
                189 => Decrease,
                51 => Sharpen,
                190 => Erase,
                45 => Insert,
                46 => Remove,
            ],
            appKeys:[
                13 => Enter,
                17 => Control,
                18 => Alt,
                16 => Shift
            ]
        }
    };

    public static function loadDefaultLimeMap():KeyboardMap{
        return {
            name: "en-GB",
            noteKeys:[
                122 => C,
                115 => CSharp,
                120 => D,
                100 => DSharp,
                99 => E,
                118 => F,
                103 => FSharp,
                98 => G,
                104 => GSharp,
                110 => A,
                106 => ASharp,
                109 => B,
                113 => CUp,
                50 => CSharpUp,
                119 => DUp,
                51 => DSharpUp,
                101 => EUp,
                114 => FUp,
                53 => FSharpUp,
                116 => GUp,
                54 => GSharpUp,
                121 => AUp,
                55 => ASharpUp,
                117 => BUp
            ],
            numberKeys:[
                48 => Num0,
                49 => Num1,
                50 => Num2,
                51 => Num3,
                52 => Num4,
                53 => Num5,
                54 => Num6,
                55 => Num7,
                56 => Num8,
                57 => Num9,
                97 => NumA,
                98 => NumB,
                99 => NumC,
                100 => NumD,
                101 => NumE,
                102 => NumF
            ],
            navigationKeys: [
                1073741906 => Up,
                1073741903 => Right,
                1073741904 => Left,
                1073741905 => Down,
                9 => Tab, 
                1073741899 => ScrollUp,
                1073741902 => ScrollDown,
                1073741898 => ScrollTop,
                1073741901 => ScrollEnd,
            ],
            alterationKeys:[
                35 => Sharpen,
                46 => Erase,
                1073741897 => Insert,
                127 => Remove,
                91 => SquareDecrease,
                93 => SquareIncrease,
                45 => Decrease,
                61 => Increase
            ],
            appKeys:[
                13 => Enter,
                1073742048 => Control,
                1073742050 => Alt,
                1073742049 => Shift
            ]
        }
    }
}
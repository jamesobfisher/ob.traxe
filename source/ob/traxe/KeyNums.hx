package ob.traxe;

enum KeyName {
	C;
	CSharp;
	D;
	DSharp;
	E;
	F;
	FSharp;
	G;
	GSharp;
	A;
	ASharp;
	B;
	CUp;
	CSharpUp;
	DUp;
	DSharpUp;
	EUp;
	FUp;
	FSharpUp;
	GUp;
	GSharpUp;
	AUp;
	ASharpUp;
	BUp;
	Sharpen;
	Decrease;
	Increase;
	SquareDecrease;
	SquareIncrease;
	Left;
	Right;
	Up;
	Down;
	Tab;
	Num0;
	Num1;
	Num2;
	Num3;
	Num4;
	Num5;
	Num6;
	Num7;
	Num8;
	Num9;
	NumA;
	NumB;
	NumC;
	NumD;
	NumE;
	NumF;
	Erase;
	Insert;
	Remove;
	ScrollUp;
	ScrollDown;
	ScrollTop;
	ScrollEnd;
	Enter;
	Control;
	Alt;
	Shift;
	NotPressed;
}

class NoteNumbers{
	static var sharps:Array<Int> = [1, 3, 6, 8, 10];

	public static function IsSharp(noteNumber:Int):Bool {
		return sharps.contains(noteNumber);
	}

	static var hasSharp:Array<Int> = [0, 2, 5, 7, 9];

	public static function CanBeSharpened(noteNumber:Int):Bool {
		return hasSharp.contains(noteNumber);
	}
}

class KeyNames {
	static var numbers:Map<KeyName, Int> = [
		Num0 => 0,
		Num1 => 1,
		Num2 => 2,
		Num3 => 3,
		Num4 => 4,
		Num5 => 5,
		Num6 => 6,
		Num7 => 7,
		Num8 => 8,
		Num9 => 9,
		NumA => 10,
		NumB => 11,
		NumC => 12,
		NumD => 13,
		NumE => 14,
		NumF => 15,
	];

	static var accidentals:Map<KeyName, Int> = [Decrease => 0, Sharpen => 1];

	public static function Accidental(keyName:KeyName):Int {
		return accidentals[keyName];
	}

	public static function Number(keyName:KeyName):Int {
		return numbers[keyName];
	}

	public static function NoteNumbers(k:KeyName):{note:Int, octave:Int} {
		var n = switch (k) {
			case CSharp: 1;
			case D: 2;
			case DSharp: 3;
			case E: 4;
			case F: 5;
			case FSharp: 6;
			case G: 7;
			case GSharp: 8;
			case A: 9;
			case ASharp: 10;
			case B: 11;
			case CUp: 12;
			case CSharpUp: 13;
			case DUp: 14;
			case DSharpUp: 15;
			case EUp: 16;
			case FUp: 17;
			case FSharpUp: 18;
			case GUp: 19;
			case GSharpUp: 20;
			case AUp: 21;
			case ASharpUp: 22;
			case BUp: 23;
			case _: 0;
		}
		var o = n >= 12 ? 1 : 0;
		return {note: n - (12 * o), octave: o};
	}
}

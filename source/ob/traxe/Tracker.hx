package ob.traxe;

import ob.seq.Metronome.ITimeContext;
import ob.seq.Sink;
import ob.traxe.Audio;
import ob.traxe.Events.KeyboardEvent;
import ob.traxe.KeyNums.KeyName;
import ob.traxe.KeyNums.KeyNames;
import ob.traxe.KeyboardMap.Keyboard;
import ob.traxe.LaneConfiguration.CursorFocus;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Time.Ticks;
import ob.traxe.Util.ReverseIterator;

typedef LaneUpdate = {entireLane:Bool, changeRowcountBy:Int};

class Tracker {
	public var navigator(default, null):Navigator;
	public var tracks(default, null):Array<Track>;
	public var onFocusChanged:(NavigatorCursor) -> Void;
	public var onValueChanged:(NavigatorCursor, LaneUpdate) -> Void;
	public var onTrackRowChanged:(Int) -> Void;

	var keyMap:KeyboardMap;
	var time:ITimeContext;
	var audio:IAudioContext;
	var ticks:Ticks;
	var sinks:Array<IPulse> = [];

	public function new(tracks:Array<Track>, timeContext:ITimeContext, audioContext:IAudioContext) {
		this.tracks = tracks;
		time = timeContext;
		audio = audioContext;
		var bpm = 114;
		ticks = new Ticks(bpm, time, scheduleTracks);
		navigator = new Navigator();
		navigator.maxTrackIndex = tracks.length - 1;
		keyMap = Keyboard.loadDefaultLimeMap();
	}

	function isInMap(keyCode:Int, map:Map<Int, KeyName>):Bool {
		var keyName = map[keyCode];
		if (keyName != null) {
			navigator.last.key = keyName;
			return true;
		}
		return false;
	}

	function isHandled(event:KeyboardEvent, focus:CursorFocus):Bool {
		if (isInMap(event.keyCode, keyMap.navigationKeys)) {
			return handledNavigation(keyMap.navigationKeys[event.keyCode], focus);
		} else if (isInMap(event.keyCode, keyMap.alterationKeys)) {
			return handledAlteration(keyMap.alterationKeys[event.keyCode], focus);
		}
		return false;
	}

	function handledAlteration(key:KeyName, focus:CursorFocus):Bool {
		var isHandled = false;
		if (key == Erase) {
			handleErase(focus);
			isHandled = true;
		} else {
			{
				trace('handle alteration');
				switch key {
					case Insert:
						// nudge
						tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).nudge();
						isHandled = true;
						valueChanged({entireLane: true, changeRowcountBy: 0});
					case Remove:
						// yank
						tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).yank();
						isHandled = true;
						valueChanged({entireLane: true, changeRowcountBy: 0});
					case SquareDecrease:
						// decrease length
						tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).changeLength(-1);
						valueChanged({entireLane: true, changeRowcountBy: -1});
					case SquareIncrease:
						// increase
						tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).changeLength(1);
						valueChanged({entireLane: true, changeRowcountBy: 1});
					case _:
						return isHandled;
				}
			}
		}
		return isHandled;
	}

	public function laneUnderCursor():Lane {
		return tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex);
	}

	function handledNavigation(key:KeyName, focus:CursorFocus):Bool {
		if (key == Tab) {
			handleTab();
			return true;
		}
		var movementDetails = {
			willMove: false,
			nextColumn: navigator.position.columnIndex,
			nextTrack: navigator.position.trackIndex,
			nextLane: navigator.position.laneIndex,
			nextRow: navigator.position.rowIndex
		}
		if (key == Up && navigator.CanMoveUp()) {
			changeCursorRow(-1);
		}
		if (key == Down && navigator.CanMoveDown(laneUnderCursor().rows.length)) {
			changeCursorRow(1);
		}
		if (key == Left) {
			// will it change column?
			if (navigator.position.columnIndex > 0) {
				movementDetails.nextColumn--;
				movementDetails.willMove = true;
			} else if (navigator.position.laneIndex > 0) {
				// or will it change lane?
				movementDetails.nextLane--;
				movementDetails.nextColumn = tracks[navigator.position.trackIndex].getLane(movementDetails.nextLane).getMaxColumnIndex();
				movementDetails.willMove = true;
			} else if (navigator.CanMoveLeft()) {
				// or will it change track
				for (i in new ReverseIterator(navigator.position.trackIndex - 1, 0)) {
					var next = tracks[i].numRows;
					if (tracks[i].numRows - 1 >= navigator.position.rowIndex) {
						movementDetails.nextTrack = i;
						movementDetails.nextLane = tracks[movementDetails.nextTrack].getMaxLaneIndex();
						movementDetails.nextColumn = tracks[movementDetails.nextTrack].getLane(movementDetails.nextLane).getMaxColumnIndex();
						movementDetails.willMove = true;
						break;
					}
				}
			}
		}
		if (key == Right) {
			// will it change column?
			if (navigator.position.columnIndex < tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).getMaxColumnIndex()) {
				movementDetails.nextColumn++;
				movementDetails.willMove = true;
			} else if (navigator.position.laneIndex < tracks[navigator.position.trackIndex].getMaxLaneIndex()) {
				// or will it change lane?
				movementDetails.nextLane++;
				movementDetails.nextColumn = 0;
				movementDetails.willMove = true;
			} else if (navigator.IsWithinTrackBounds()) {
				// or will it change track
				for (i in movementDetails.nextTrack + 1...tracks.length) {
					if (tracks[i].numRows - 1 >= navigator.position.rowIndex) {
						movementDetails.nextTrack = i;
						movementDetails.nextLane = 0;
						movementDetails.nextColumn = 0;
						movementDetails.willMove = true;
						break;
					}
				}
			}
		}

		if (movementDetails.willMove) {
			navigator.position.rowIndex = movementDetails.nextRow;
			navigator.position.columnIndex = movementDetails.nextColumn;
			navigator.position.laneIndex = movementDetails.nextLane;
			navigator.position.trackIndex = movementDetails.nextTrack;
			focusChanged();
		}

		return movementDetails.willMove;
	}

	public function handleKeyDown(event:KeyboardEvent) {
		#if debug
		trace('keyCode: ${event.keyCode}');
		#end
		navigator.last.keyCode = event.keyCode;
		var focus = getFocusConfiguration();
		if (!isHandled(event, focus)) {
			switch (focus.column) {
				case NoteName:
					handleIfValidEditKey(event, keyMap.noteKeys, handleNote);
				case Accidental:
					handleIfValidEditKey(event, keyMap.alterationKeys, handleAccidental);
				case Octave:
					handleIfValidEditKey(event, keyMap.numberKeys, handleOctave);
				case Hex:
					handleIfValidEditKey(event, keyMap.numberKeys, handleParameter);
				case Switch:
					handleIfValidEditKey(event, keyMap.numberKeys, handleParameter);
				case _:
					return;
			}
		}
	}

	function handleIfValidEditKey(event:KeyboardEvent, keyMap:Map<Int, KeyName>, handler:(KeyName) -> Void) {
		if (isInMap(event.keyCode, keyMap)) {
			handler(keyMap[event.keyCode]);
		}
	}

	function getFocusConfiguration():CursorFocus {
		return tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).getFocusConfiguration(navigator.position.columnIndex);
	}

	function handleNote(key:KeyName) {
		if (tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).setNote(navigator.position, navigator.last)) {
			valueChanged();
		}
		advanceFocusedRow();
	}

	function handleAccidental(key:KeyName) {
		var value = KeyNames.Accidental(key);
		if (tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).setValue(value, navigator.position, navigator.last)) {
			valueChanged();
		}
		advanceFocusedRow();
	}

	function handleOctave(key:KeyName) {
		var value = KeyNames.Number(key);
		if (value >= 9)
			return;
		// todo check high value{
		if (tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).setValue(value, navigator.position, navigator.last)) {
			valueChanged();
		}
		advanceFocusedRow();
	}

	function handleParameter(key:KeyName) {
		var value = KeyNames.Number(key);
		if (tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).setValue(value, navigator.position, navigator.last)) {
			valueChanged();
		}
		advanceFocusedRow();
	}

	function handleErase(focus:CursorFocus) {
		tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex).erase(navigator.position.rowIndex);
		valueChanged();
		advanceFocusedRow();
	}

	function changeCursorRow(direction:Int) {
		navigator.position.rowIndex += direction;
		focusChanged();
	}

	function focusChanged() {
		if (onFocusChanged != null) {
			onFocusChanged(navigator.position);
		}
	}

	function valueChanged(update:LaneUpdate = null) {
		if (onValueChanged != null) {
			var toUpdate = update == null ? {entireLane: false, changeRowcountBy: 0} : update;
			onValueChanged(navigator.position, toUpdate);
		}
	}

	function advanceFocusedRow() {
		if (navigator.CanMoveDown(laneUnderCursor().rows.length)) {
			changeCursorRow(1);
		}
	}

	function handleTab() {
		if (navigator.IsWithinTrackBounds()) {
			navigator.position.trackIndex++;
			navigator.position.laneIndex = 0;
			navigator.position.columnIndex = 0;
			focusChanged();
		}
	}

	public function information():String {
		var lane = tracks[navigator.position.trackIndex].getLane(navigator.position.laneIndex);
		return '[${tracks[navigator.position.trackIndex].name}] [${lane.laneType}]';
	}

	public function handleTrackRowHighlightChange(trackIndex:Int) {
		onTrackRowChanged(trackIndex);
	}

	public function toggleIsStarted() {
		ticks.toggleIsStarted();
	}

	public function isStarted():Bool {
		return ticks.isStarted();
	}

	function scheduleTracks() {
		for (trackIndex => track in tracks) {
			// todo set up more robust track to sink relations
			#if web
			var r = tracks[trackIndex].getLane(0).getValue(tracks[trackIndex].playIndex);
			if (r > 0) {
				sinks[trackIndex].gain = r / 140;
				#if debug
				trace('gain $trackIndex ${sinks[trackIndex].gain}');
				#end
				sinks[trackIndex].pulse(ticks.schedule.nextDivisionTime);
			}
			#end
			track.advancePlayIndex();
			handleTrackRowHighlightChange(trackIndex);
		}
	} // todo move into traxe

	public function addPulseSink(sink:IPulse) {
		sinks.push(sink);
	}
}

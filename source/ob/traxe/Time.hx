package ob.traxe;

import ob.seq.Metronome.DeltaTimerMetronome;
import ob.seq.Metronome.ISchedule;
import ob.seq.Metronome.ITick;
import ob.seq.Metronome.ITimeContext;
#if web
import ob.seq.WebMetronome.WebAudioMetronome;
#end

class Ticks implements ITick {
	public var schedule(default, null):ISchedule;
	public var time(default, null):ITimeContext;
	public var onTick:() -> Void;

	public function new(bpm:Float, timeContext:ITimeContext, onTick:() -> Void) {
		#if !web
		schedule = new DeltaTimerMetronome(bpm, timeContext);
		#else
		schedule = new WebAudioMetronome(bpm, timeContext);
		#end
		this.onTick = onTick;
		schedule.onDivision = this.onTick;
	}

	public function toggleIsStarted():Void {
		#if debug
		trace('toggle is started');
		#end
		schedule.toggleStarted();
	}

	public function isStarted():Bool {
		return schedule.isStarted;
	}
}

package ob.traxe;

import ob.parameters.ParameterType;

enum LaneType {
	Note;
	Parameter;
	Switch;
}

enum ColumnType {
	Empty;
	Hex;
	NoteName;
	Accidental;
	Octave;
	Switch;
}

typedef CursorFocus = {var lane:LaneType; var column:ColumnType;}

class LaneConfiguration {
	public static function NumberOfColumns(type:LaneType):Int {
		return switch (type) {
			case Note: 3;
			case Switch: 1;
			case _: 2;
		}
	}

	public static function ToParameterType(laneType:LaneType):ParameterType {
		return switch (laneType) {
			case Note: NOTE;
			case _: CC;
		}
	}

	public static function ToLaneType(parameterType:ParameterType):LaneType {
		return switch (parameterType) {
			case NOTE: Note;
			case _: Parameter;
		}
	}
}

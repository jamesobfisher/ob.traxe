package ob.traxe;

import ob.traxe.Lane;
import ob.traxe.LaneConfiguration.LaneType;
import ob.traxe.ParameterDefinitions.ParameterLaneDef;

class Track {
	public var index(default, null):Int;
	public var lanes(default, null):Array<Lane>;
	public var laneTypes(default, null):Array<LaneType>;
	public var laneNames(default, null):Array<String>;
	public var name(default, null):String;
	public var playIndex(default, null):Int = 0;
	public var numRows(get, null):Int;

	public function get_numRows():Int {
		return lanes[0].rows.length;
	}

	public function new(trackIndex:Int, name:String = "", laneDefs:Array<ParameterLaneDef>, numRows:Int) {
		this.index = trackIndex;
		this.name = name;
		this.numRows = numRows;
		lanes = [];
		laneTypes = [];
		laneNames = [];
		var laneIndex = 0;
		for (l in laneDefs) {
			var lane = new Lane(laneIndex, l.parameter, numRows);
			laneNames.push(l.parameter.name);
			laneTypes.push(l.laneType);
			lanes.push(lane);
			laneIndex++;
		}
	}

	public function getLane(laneIndex:Int):Lane {
		return lanes[laneIndex];
	}

	public function getMaxLaneIndex():Int {
		return lanes.length - 1;
	}

	public function advancePlayIndex() {
		playIndex++;
		if (playIndex >= lanes[0].rows.length) {
			playIndex = 0;
		}
	}

	public function resetPlayIndex(){
		playIndex = 0;
	}
}

package ob.traxe;

class StringFormatting {
	public static function toHexString():String {
		return "";
	}

	public static function formatHexString(v:Dynamic):String {
		var n:Int = cast v;
		return n < 0 ? "." : '${StringTools.hex(v)}';
	}

	public static function formatAsString(v:Dynamic):String {
		return '${v}';
	}

	static var notes:Map<Int, String> = [
		0 => "C",
		1 => "C",
		2 => "D",
		3 => "D",
		4 => "E",
		5 => "F",
		6 => "F",
		7 => "G",
		8 => "G",
		9 => "A",
		10 => "A",
		11 => "B",
		// 0 => "C", 1 => "C", 2 => "D", 3 => "D#", 4 => "E", 5 => "F", 6 => "F#", 7 => "G", 8 => "G#", 9 => "A", 10 => "A#", 11 => "B",
	];

	public static function formatNote(n:Int) {
		return n < 0 ? "." : notes[n];
	}

	public static function formatAccidental(v:Int) {
		return v == 1 ? "#" : "-";
	}
}

package ob.traxe;

class MathUtil{
    static public function SumHex(hexValues:Array<Int>):Int{
		var _:Float = 0;
		var len = hexValues.length - 1;
		for(i in 0...hexValues.length){
			_ +=  hexValues[i]*Math.pow(16,len);
			len--;
		}
		return Math.round(_);
	}

    // static public function SumHex(hexValues:Array<Int>):Int{
	// 	var _:Float = 0;
    //     for(i in new ReverseIterator(hexValues.length - 1, 0)){
    //         _ +=  hexValues[i]*Math.pow(16,i);
    //     }
	// 	return Math.round(_);
	// }
}

class ReverseIterator {
    var end:Int;
    var i:Int;
  
    public inline function new(start:Int, end:Int) {
      this.i = start;
      this.end = end;
    }
  
    public inline function hasNext() return i >= end;
    public inline function next() return i--;
  }

  class ArrayUtil{
    public static function rotate<T>(arr:Array<T>, k:Int) {
      var l = arr.length;
      reverse(arr, 0, l - 1);
      reverse(arr, 0, k - 1);
      reverse(arr, k, l - 1);
    }

    static function reverse<T>(arr:Array<T>, left:Int, right:Int){
      while (left < right) {
          var temp = arr[left];
          arr[left] = arr[right];
          arr[right] = temp;
          left++;
          right--;
      }
    }

    public static function copyLeft<T>(arr:Array<T>, fillWith:T=null){
      var temp = arr.slice(1, arr.length);
      temp.push(fillWith);
      for(i => t in temp){
        arr[i] = t;
      }
    }
  }

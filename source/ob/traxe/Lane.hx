package ob.traxe;

import ob.traxe.Navigator.LastPress;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Util.MathUtil;
import ob.parameters.Parameter;
import ob.traxe.KeyNums.KeyNames;
import ob.traxe.KeyNums.NoteNumbers;
import ob.traxe.LaneConfiguration.ColumnType;
import ob.traxe.LaneConfiguration.CursorFocus;
import ob.traxe.LaneConfiguration.LaneType;
import ob.traxe.StringFomatting.StringFormatting;

using ob.traxe.Util;

class Lane {
	public var rows(default, null):Array<Null<Int>>;
	public var numRows(default, null):Int;
	public var numColumns(default, null):Int;
	public var index(default, null):Int;
	public var laneType(default, null):LaneType;
	public var columnTypes(default, null):Array<ColumnType>;
	public var columns(default, null):Array<Array<Int>>;

	var parameter:Parameter;

	public function new(index:Int, parameter:Parameter, numRows:Int) {
		this.index = index;
		this.parameter = parameter;
		this.numRows = numRows;
		laneType = LaneConfiguration.ToLaneType(parameter.type);
		numColumns = LaneConfiguration.NumberOfColumns(laneType);
		columnTypes = switch (laneType) {
			case Note: [NoteName, Accidental, Octave];
			case Switch: [Switch];
			case _: [Hex, Hex];
		}
		columns = [for (i in 0...numColumns) [for (i in 0...numRows) -1]];
		rows = [for (i in 0...numRows) null];
	}

	public function getFocusConfiguration(columnIndex:Int):CursorFocus {
		return {
			lane: laneType,
			column: columnTypes[columnIndex]
		};
	}

	public function getMaxColumnIndex():Int {
		return numColumns - 1;
	}

	public function getValue(rowIndex:Int):Int {
		return rows[rowIndex];
	}

	public function setValue(value:Int, position:NavigatorCursor, pressed:LastPress):Bool {
		var valueChanged = false;
		if (columns[position.columnIndex][position.rowIndex] != value) {
			columns[position.columnIndex][position.rowIndex] = value;
			valueChanged = true;
			var values = [];
			for (i in 0...numColumns) {
				if (columns[i][position.rowIndex] < 0) {
					columns[i][position.rowIndex] = defaultValue(columnTypes[i], pressed);
				}
				values.push(columns[i][position.rowIndex]);
			}
			rows[position.rowIndex] = values.SumHex();
		}
		return valueChanged;
	}

	public function setNote(position:NavigatorCursor, pressed:LastPress):Bool {
		var n = KeyNames.NoteNumbers(pressed.key);
		var valueChanged = setValue(n.note, position, pressed);
		var octave = pressed.octave + n.octave;
		var octaveIndex = columnTypes.indexOf(Octave);
		if (columns[octaveIndex][position.rowIndex] != octave) {
			columns[octaveIndex][position.rowIndex] = octave;
			valueChanged = true;
		}
		var newNoteIsSharp = NoteNumbers.IsSharp(n.note);
		var accidentalIndex = columnTypes.indexOf(Accidental);
		var sharpValue = newNoteIsSharp ? 1 : 0;
		if (columns[accidentalIndex][position.rowIndex] != sharpValue) {
			columns[accidentalIndex][position.rowIndex] = sharpValue;
			valueChanged = true;
		}
		return valueChanged;
	}

	public function erase(rowIndex:Int) {
		for (i in 0...numColumns) {
			columns[i][rowIndex] = -1;
		}
		rows[rowIndex] = 0;
	}

	public function formatRow(rowIndex:Int):Array<{char:String, column:ColumnType}> {
		return [
			for (i in 0...numColumns)
				{char: format(columnTypes[i], columns[i][rowIndex]), column: columnTypes[i]}
		];
	}

	function format(columnType:ColumnType, value:Int) {
		if (value < 0) {
			return ".";
		}
		return switch (columnType) {
			case Hex: StringFormatting.formatHexString(value);
			case NoteName: StringFormatting.formatNote(value);
			case Accidental: StringFormatting.formatAccidental(value);
			case _: Std.string(value);
		}
	}

	function determineOctave(pressed:LastPress):Int {
		var _ = KeyNames.NoteNumbers(pressed.key);
		return _.octave + pressed.octave;
	}

	function defaultValue(columnType:ColumnType, pressed:LastPress):Int {
		return switch (columnType) {
			case Octave: determineOctave(pressed);
			case _: 0;
		}
	}

	public function nudge() {
		#if debug
		trace('nudge');
		#end
		rows.rotate(1);
		for (c in columns) {
			c.rotate(1);
		}
	}

	public function yank() {
		#if debug
		trace('yank');
		#end
		// rows.copyLeft();
		// for (c in columns) {
		// 	c.copyLeft(-1);
		// }
		// todo - be more efficient
		rows.reverse();
		rows.rotate(1);
		rows.reverse();
		for (c in columns) {
			c.reverse();
			c.rotate(1);
			c.reverse();
		}
	}

	public function changeLength(by:Int){
		if(rows.length + by == 0){
			return;
		}
		if(by < 0){
			// reduce
			rows = rows.slice(0, rows.length + by);
			for(i in 0...columns.length){
				columns[i] = columns[i].slice(0, columns[i].length + by);
			}
		}
		else{
			// increase
			for(i in 0...by){
				rows.push(null);
			}
			for(c in columns){
				for(i in 0...by){
					c.push(-1);
				}
			}
		}
	}
}

package ob.traxe;

import ob.traxe.LaneConfiguration.LaneType;
import ob.parameters.Parameter;

typedef ParameterLaneDef = {parameter:Parameter, laneType:LaneType};

class ParameterDefinitions {
	public static var map(default, null):Map<Int, ParameterLaneDef> = [
		1 => { parameter: new Parameter("Note", 1, NOTE), laneType: Note},
		2 => { parameter: new Parameter("Velocity", 2, VELOCITY), laneType: Parameter}
	];
}

package ob.traxe;

import ob.traxe.LaneConfiguration.LaneType;

enum TrackType{
    MidiNote;
    MidiCC;
    MidiSysEx;
}

class TrackConfiguration{
    // public static function TrackFor(trackIndex:Int, type:TrackType):Track{
    //     return new Track(trackIndex, laneTypes(type), laneNames(type));
    // }

    static function laneTypes(type:TrackType):Array<LaneType>{
        return switch (type){
            case MidiNote: [Note, Parameter];
            case _: [Parameter];
        }
    }

    static function laneNames(type:TrackType):Array<String>{
        return switch (type){
            case MidiNote: ["Note", "Velocity"];
            case _: ["CC"];
        }
    }
}
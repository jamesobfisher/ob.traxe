package ob.traxe;

import ob.traxe.KeyNums.KeyName;

typedef NavigatorCursor = {trackIndex:Int, laneIndex:Int, columnIndex:Int, rowIndex:Int}
typedef LastPress = {key:KeyName, octave:Int, keyCode:Int}

class Navigator {
	public function new() {}

	public var maxRowIndex:Int = 15;
	public var maxTrackIndex:Int = 0;
	public var position(default, null):NavigatorCursor = {
		trackIndex: 0,
		laneIndex: 0,
		columnIndex: 0,
		rowIndex: 0
	};
	public var last(default, null):LastPress = {key: KeyName.NotPressed, octave: 3, keyCode: 0};

	public function CanMoveUp():Bool {
		return position.rowIndex > 0;
	}

	public function CanMoveDown(numElementsInRow:Int):Bool {
		return position.rowIndex < numElementsInRow - 1;
	}

	public function CanMoveLeft():Bool {
		return position.trackIndex > 0;
	}

	public function IsWithinTrackBounds():Bool {
		return position.trackIndex < maxTrackIndex;
	}
}

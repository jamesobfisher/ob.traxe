package tests;


import ob.traxe.Util.MathUtil;
import massive.munit.Assert;
using equals.Equal;



class MathTests {

	@Test
	function SumFF(){
		Assert.areEqual(MathUtil.SumHex([0xf,0xf]), 0xff);
	}
	
	@Test
	function Sum0F(){
		Assert.areEqual(MathUtil.SumHex([0x0,0xf]), 0x0f);
	}

	@Test
	function SumFFF(){
		Assert.areEqual(MathUtil.SumHex([0xf,0xf,0xf]), 0xfff);
	}

	@Test
	function SumFAFAF(){
		Assert.areEqual(MathUtil.SumHex([0xf,0xa,0xf,0xa,0xf]), 0xfafaf);
	}
}

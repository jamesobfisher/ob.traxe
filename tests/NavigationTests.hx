package tests;

import ob.traxe.TrackerConfiguration;
import ob.traxe.Track;
import ob.traxe.Tracker;
import massive.munit.Assert;

/**
todo - these tests do not check column and lane positions are correct, which is important to test as well
**/

class NavigationTests {
	@Test
	public function SkipColumnGapToRight() {
		var tracks = [
			new Track(0, "", TrackerConfiguration.getParameters([2]), 8),
			new Track(1, "", TrackerConfiguration.getParameters([2]), 1),
			new Track(2, "", TrackerConfiguration.getParameters([2]), 8),
		];
		var tracker = new Tracker(tracks);
		tracker.navigator.position.rowIndex = 3;
		tracker.navigator.position.columnIndex = 1;
		tracker.handleKeyDown({keyCode: 1073741903, modifier: 0});
		Assert.areEqual(tracker.navigator.position.trackIndex, 2);
	}

	@Test
	public function SkipColumnGapToLeft() {
		var tracks = [
			new Track(0, "", TrackerConfiguration.getParameters([2]), 8),
			new Track(1, "", TrackerConfiguration.getParameters([2]), 1),
			new Track(2, "", TrackerConfiguration.getParameters([2]), 8),
		];
		var tracker = new Tracker(tracks);
		tracker.navigator.position.rowIndex = 3;
		tracker.navigator.position.trackIndex = 2;
		tracker.handleKeyDown({keyCode: 1073741904, modifier: 0});
		Assert.areEqual(tracker.navigator.position.trackIndex, 0);
	}
}

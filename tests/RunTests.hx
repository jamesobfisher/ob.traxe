import tests.NavigationTests;
import tests.LaneTests;
import tests.TrackTests;
import tests.TrackConfigurationTests;
import tests.LaneConfigurationTests;
import tests.MathTests;

import massive.munit.client.RichPrintClient;
import massive.munit.TestRunner;
import massive.munit.Assert;

class RunTests {
	static function main() {
		var client = new RichPrintClient();
		var runner = new TestRunner(client);
		runner.run([TestSuite]);
	}
}

class TestSuite extends massive.munit.TestSuite {
	public function new() {
		super();
		add(NavigationTests);
		add(TrackConfigurationTests);
		add(LaneConfigurationTests);
		add(MathTests);
		add(LaneTests);
		add(TrackTests);
	}
}

class TestCase {
	@Test
	function testSuccess() {
		Assert.isTrue(true);
	}

	@Test
	function testFailure() {
		Assert.areEqual("A", "B");
	}

	@Test
	function testError() {
		throw "error";
	}

	@Test
	function testEmpty() {}

	@Test @Ignore("Description")
	function testIgnore() {}
}

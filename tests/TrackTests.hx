package tests;

import ob.traxe.TrackerConfiguration;
import ob.traxe.Track;
import massive.munit.Assert;

using equals.Equal;

class TrackTests {
	@Test
	public function AdvancePlayIndexDefault() {
		var config = TrackerConfiguration.getParameters([2]);
		var track = new Track(0, "", config, 16);
		Assert.areEqual(0, track.playIndex, "because nothing has moved so we are still on index 0");
	}

	@Test
	public function AdvancePlayIndex() {
		var config = TrackerConfiguration.getParameters([2]);
		var track = new Track(0, "", config, 16);
		track.advancePlayIndex();
		Assert.areEqual(1, track.playIndex, "because we moved 1 position from 0");
	}

	@Test
	public function AdvancePlayIndexWraps() {
		var config = TrackerConfiguration.getParameters([2]);
		var track = new Track(0, "", config, 16);
		for (i in 0...track.numRows - 1) {
			track.advancePlayIndex();
		}
		Assert.areEqual(15, track.playIndex, "because we are on the final row");
		track.advancePlayIndex();
		Assert.areEqual(0, track.playIndex, "because we passed the final row and wrapped to position 1");
	}
}

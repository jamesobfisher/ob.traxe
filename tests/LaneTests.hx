package tests;

import ob.traxe.Navigator;
import ob.parameters.ParameterType;
import ob.traxe.Lane;
import ob.parameters.Parameter;
import massive.munit.Assert;

using equals.Equal;

class LaneTests {
	@Test
	public function Nudge() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 16);
		var navigator = new Navigator();
		navigator.position.columnIndex = 1;
		for (i in 10...16) {
			navigator.position.rowIndex = i;
			lane.setValue(i, navigator.position, navigator.last);
		}
		// var initialStates = [for(i in 0...lane.rows.length) {v:lane.rows[i], row:i}];
		// for (row in initialStates) {
		//     trace(row);
		// }
		lane.nudge();
		Assert.areEqual(lane.rows[0], 15, "because the data on the last row should now be at the beginning");
		Assert.areEqual(lane.columns[1][0], 15, "because the columns should reflect the rows");
	}

	@Test
	public function Yank() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 16);
		var navigator = new Navigator();
		navigator.position.columnIndex = 1;
		for (i in 10...16) {
			navigator.position.rowIndex = i;
			lane.setValue(i, navigator.position, navigator.last);
		}
		// traceArray(lane.rows);
		lane.yank();
		// traceArray(lane.rows);
		Assert.areEqual(lane.rows[14], 15, "because the data on the last row should now be in the row before");
		Assert.isNull(lane.rows[15], "because the data has been shifted, leaving an empty space");
		Assert.areEqual(lane.columns[1][15], -1, "because the data has been shifted, leaving -1 in the final item");
	}

	@Test
	public function YankWraps() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 16);
		var navigator = new Navigator();
		navigator.position.columnIndex = 1;
		for (i in 0...6) {
			navigator.position.rowIndex = i;
			lane.setValue(i, navigator.position, navigator.last);
		}
		// traceArray(lane.rows);
		lane.yank();
		// traceArray(lane.rows);
		Assert.areEqual(lane.rows[15], 0, "because the data on the first row should now have wrapped to the final row");
	}

	@Test
	public function IncreaseLength() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 5);
		lane.changeLength(1);
		// Assert.areEqual(lane.rows.length, 6, "because there were 5 rows to begin and then 1 was added");
		Assert.areEqual(lane.rows.length, 6);
		for(r in lane.rows){
			Assert.isNull(r);
		}
		for(c in lane.columns){
			// Assert.areEqual(c.length, 6, "because there were 5 rows to begin and then 1 was added");
			Assert.areEqual(c.length, 6);
			for(r in c){
				Assert.areEqual(-1, r);
			}
		}
	}

	@Test
	public function ReduceLength() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 10);
		lane.changeLength(-1);
		Assert.areEqual(lane.rows.length, 9);//"because there were 10 rows to begin and then 1 was removed"
		for(c in lane.columns){
			Assert.areEqual(c.length, 9); //"because there were 10 rows to begin and then 1 was added"
		}
	}


	@Test
	public function ReduceLengthStopsAtOne() {
		var lane = new Lane(0, new Parameter("", 0, ParameterType.VELOCITY), 1);
		lane.changeLength(-1);
		Assert.areEqual(lane.rows.length, 1, "because there must always be 1 row it cannot be reduced");
		for(c in lane.columns){
			Assert.areEqual(c.length, 1, "because there must always be 1 row it cannot be reduced");
		}
	}



	function traceArray<T>(arr:Array<T>) {
		var a = [for (i in 0...arr.length) {v: arr[i], row: i}];
		for (_ in a) {
			trace(_);
		}
	}

	
}

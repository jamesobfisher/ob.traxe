package tests;

import ob.traxe.LaneConfiguration;
import massive.munit.Assert;
using equals.Equal;

class LaneConfigurationTests {
	@Test
	function NoteLaneHas3Columns() {
		var numColumns = LaneConfiguration.NumberOfColumns(Note);
		Assert.areEqual(numColumns, 3);
	}

	@Test
	function ParameterLaneHas2Columns() {
		var numColumns = LaneConfiguration.NumberOfColumns(Parameter);
		Assert.areEqual(numColumns, 2);
	}
	
	@Test
	function SwitchLaneHas1Column() {
		var numColumns = LaneConfiguration.NumberOfColumns(Switch);
		Assert.areEqual(numColumns, 1);
	}
}
